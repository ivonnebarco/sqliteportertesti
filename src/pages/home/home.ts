import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  developer = {};
  developers = [];
  tiposector = {};

  constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private platform: Platform) {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.loadDeveloperData();
      }
    })
  }
 
  loadDeveloperData() {
    this.databaseprovider.getAllDevelopers().then(data => {
      this.developers = data;
    })
  }

  loadTipoSectorData() {
    this.databaseprovider.getAllTipoSectores().then(data => {
      this.developers = data;
    })
  }
 
  addTipoSector() {
    this.databaseprovider.addTipoSector(this.tiposector['pkidtiposector'], this.tiposector['codigotiposector'], this.tiposector['nombretiposector'], this.tiposector['tiposectoractivo'], this.tiposector['creaciontiposector'], this.tiposector['modificaciontiposector'], this.tiposector['descripciontiposector'])
    .then(data => {
      this.loadDeveloperData();
    });
    this.developer = {};
  }


  addDeveloper() {
    this.databaseprovider.addTipoSector(this.tiposector['pkidtiposector'], this.tiposector['codigotiposector'], this.tiposector['nombretiposector'], this.tiposector['tiposectoractivo'], this.tiposector['creaciontiposector'], this.tiposector['modificaciontiposector'], this.tiposector['descripciontiposector'])
    .then(data => {
      this.loadDeveloperData();
    });
    this.developer = {};
  }

}
