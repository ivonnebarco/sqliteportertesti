import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

/**
 * Generated class for the SectoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sectores',
  templateUrl: 'sectores.html',
})
export class SectoresPage {

  developer = {};
  developers = [];
  tiposector = {};

  constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private platform: Platform) {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.loadTipoSectorData();
      }
    })
  }


  loadTipoSectorData() {
    this.databaseprovider.getAllTipoSectores().then(data => {
      this.developers = data;
    })
  }

  addTipoSector() {
    this.databaseprovider.addTipoSector(this.tiposector['pkidtiposector'], this.tiposector['codigotiposector'], this.tiposector['nombretiposector'], this.tiposector['tiposectoractivo'], this.tiposector['creaciontiposector'], this.tiposector['modificaciontiposector'], this.tiposector['descripciontiposector'])
      .then(data => {
        this.loadTipoSectorData();
      });
    this.developer = {};
  }

}
